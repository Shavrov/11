package org.example.application.auth.menu;

import lombok.RequiredArgsConstructor;
import org.example.application.auth.AuthService;
import org.example.application.auth.User;
import org.example.application.auth.UserView;
import org.example.application.infrastructure.menu.MenuItem;

@RequiredArgsConstructor
public class RegisterMenuItem implements MenuItem {

    private final AuthService authService;
    private final UserView userView;

    @Override
    public String getName() {
        return "Register";
    }

    @Override
    public void run() {
        User user = userView.readUser();
        try {
            authService.register(user);
        } catch (IllegalArgumentException exception) {
            userView.showError(exception.getMessage());
        }
    }

    @Override
    public boolean isVisible() {
        return !authService.isAuth();
    }
}
