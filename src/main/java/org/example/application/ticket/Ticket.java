package org.example.application.ticket;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Ticket {
    private String id;
    private String email;
    private String issue;
    private String comment;
    private String status;
    private String createdUser;
    private String assigneeUser;





}
