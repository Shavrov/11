package org.example.application.ticket;

import lombok.RequiredArgsConstructor;
import org.example.application.auth.AuthService;
import org.example.application.infrastructure.ui.ViewHelper;

import java.util.Optional;
import java.util.UUID;
import java.util.function.Supplier;
import java.util.stream.Stream;

@RequiredArgsConstructor
public class TicketView {
    private final ViewHelper viewHelper;
    private final AuthService authService;
    private final TicketService ticketService;

    public Ticket readTicket() {
        String id = UUID.randomUUID().toString();
        String email = viewHelper.readString("Enter your email: ");
        String issue = viewHelper.readString("Enter your issue: ");
        String comment = "";
        String status = "new";
        String createdUser = authService.current().get().getLogin();
        String assigneeUser = "";
        return new Ticket(id, email, issue, comment, status, createdUser, assigneeUser);
    }
    public void showAllTickets() {
        Supplier<Stream<Ticket>> tickets = ticketService::getAllTickets;
        printTickets(tickets);
    }
    public String readTicketId() {
        return viewHelper.readString("Enter ticket id: ");
    }

    public void searchTicketById(String id) {
        Optional<Ticket> ticket = ticketService.findTicketById(id);
        if (ticket.equals(null)) {
            System.out.println("Ticket by Id don't found");
        }
        else System.out.println(ticket.get());
    }
    public void showMyCreatedTickets() {
        String login = authService.current().get().getLogin();
        Supplier<Stream<Ticket>> tickets = () -> ticketService.filterByCreatedUser(login);
        printTickets(tickets);
    }


    public void showMyAssignedTickets() {
        String login = authService.current().get().getLogin();
        Supplier <Stream<Ticket>> tickets =() -> ticketService.filterByAssigneeUser(login);
        printTickets(tickets);
    }
    public void showTicketsByStatus() {
        String status = viewHelper.readString("Enter status - new, in progress or done: ");
        Supplier<Stream<Ticket>> tickets=() -> ticketService.filterTicketByStatus(status);
        printTickets(tickets);
    }

    public String readStatus() {
        return viewHelper.readString("Enter status - new, in progress or done: ");
    }

    public String readComment() {
        return viewHelper.readString("Enter comment: ");
    }

    public String readUser() {
        return viewHelper.readString("Enter user login: ");
    }
    private void printTickets(Supplier<Stream<Ticket>> tickets) {
        if (tickets.get().count() > 0) {
            tickets.get().forEach(System.out::println);
        } else {
            System.out.println("0 tickets found");
        }
    }

}
