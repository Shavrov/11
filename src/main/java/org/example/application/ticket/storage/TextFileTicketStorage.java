package org.example.application.ticket.storage;

import lombok.RequiredArgsConstructor;
import org.example.application.ticket.Ticket;
import org.example.application.ticket.TicketStorage;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

@RequiredArgsConstructor

public class TextFileTicketStorage implements TicketStorage {
    private final String filePath;
    @Override
    public Stream<Ticket> getAll() {
        try {
            return Files.lines(Path.of(filePath)).filter(line -> !line.trim().startsWith("#")).filter(line -> !line.trim().isEmpty()).map(this::extractTicket);
        } catch (IOException e) {
            return Stream.empty();
        }

    }



    @Override
    public void save(Ticket ticket) {
        try {
            Files.write(Path.of(filePath), toLine(ticket), StandardOpenOption.WRITE, StandardOpenOption.CREATE, StandardOpenOption.APPEND);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }


    @Override
    public void update(Optional<Ticket> ticket) {
        getAll().map(t -> {
            if (t.getId().equals(ticket.get().getId())) {
                return ticket.get();
            } else return t;
        }).map(this::toLine).forEach(ticketLine -> {
            try {
                Files.write(Path.of(filePath + "_tmp"), ticketLine, StandardOpenOption.WRITE, StandardOpenOption.CREATE, StandardOpenOption.APPEND);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
        try {
            Files.delete(Path.of(filePath));
            Files.move(Path.of(filePath + "_tmp"), Path.of(filePath));
        } catch (IOException e) {
            e.printStackTrace();
        }


    }
    private Ticket extractTicket(String s) {
        String[] parts = s.split("\\s\\|\\s");
        return new Ticket(parts[0], parts[1], parts[2], parts[3], parts[4], parts[5], parts[6]);
    }

    private Iterable<String> toLine(Ticket ticket) {
        return List.of(ticket.getId() + " | " + ticket.getEmail() + " | " + ticket.getIssue() + " | " + ticket.getComment() + " | " + ticket.getStatus() + " | " + ticket.getCreatedUser() + " | " + ticket.getAssigneeUser() + " | ");
    }


}
