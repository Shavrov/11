package org.example.application.ticket.menu;


import lombok.RequiredArgsConstructor;
import org.example.application.auth.AuthService;
import org.example.application.infrastructure.menu.MenuItem;
import org.example.application.ticket.TicketService;
import org.example.application.ticket.TicketView;

@RequiredArgsConstructor

public class ChangeTicketAssigneeMenuItem implements MenuItem {

    private final AuthService authService;
    private final TicketView ticketView;

    private final TicketService ticketService;


    @Override
    public String getName() {
        return "Assign Ticket";
    }

    @Override
    public void run() {
        String id = ticketView.readTicketId();
        ticketView.searchTicketById(id);;
        String userLogin = ticketView.readUser();
        ticketService.assignUser(id, userLogin);

    }

    @Override
    public boolean isVisible() {
        return authService.isAuth();
    }
}
