package org.example.application.ticket.menu;

import lombok.RequiredArgsConstructor;
import org.example.application.auth.AuthService;
import org.example.application.infrastructure.menu.MenuItem;
import org.example.application.ticket.Ticket;
import org.example.application.ticket.TicketService;
import org.example.application.ticket.TicketView;
@RequiredArgsConstructor
public class AddTicketMenuItem implements MenuItem {
    private final AuthService authService;
    private final TicketView ticketView;
    private final TicketService ticketService;

    @Override
    public String getName() {
        return "Add Ticket";
    }

    @Override
    public void run() {
        Ticket ticket = ticketView.readTicket();
        ticketService.addTicket(ticket);
    }

    @Override
    public boolean isVisible() {
        return authService.isAuth();
    }
}
