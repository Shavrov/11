package org.example.application.ticket.menu;

import lombok.RequiredArgsConstructor;
import org.example.application.auth.AuthService;
import org.example.application.infrastructure.menu.MenuItem;
import org.example.application.ticket.TicketView;

@RequiredArgsConstructor
public class AllTicketsMenuItem implements MenuItem {
    private final AuthService authService;
    private final TicketView ticketView;

    @Override
    public String getName() {
        return  "Show All Tickets";
    }

    @Override
    public void run() {
        ticketView.showAllTickets();
    }

    @Override
    public boolean isVisible() {
        return authService.isAuth();
    }
}
