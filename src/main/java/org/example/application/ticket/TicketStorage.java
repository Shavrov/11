package org.example.application.ticket;

import java.util.Optional;
import java.util.stream.Stream;

public interface TicketStorage {
    Stream<Ticket> getAll();
    void save (Ticket ticket);
    void update (Optional <Ticket> ticket);

}
